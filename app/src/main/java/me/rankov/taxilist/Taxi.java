package me.rankov.taxilist;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Taxi {

    private String name;
    private Drawable icon;
    private int eta;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }

    public int getEta() {
        return eta;
    }

    public void setEta(int eta) {
        this.eta = eta;
    }

    public static List<Taxi> createStubList() {
        Drawable icon = new ColorDrawable(Color.BLUE);
        String[] names = {"Castle", "Aviv", "Ichilov", "Eilat", "Bograshov", "Beit Hachayal",
                "Balfour", "Gordon", "Dan", "Habima", "Blue House", "Merkaz", "Hameshuchrar",
                "Train", "Masarik", "Ron", "Kochav", "New York", "Idan"};
        int maxEta = (int) TimeUnit.HOURS.toMinutes(2);

        List<Taxi> taxis = new ArrayList<>();
        for (int i = 1; i < names.length; i++) {
            Taxi taxi = new Taxi();
            taxi.setName(names[i]);
            taxi.setIcon(icon);
            taxi.setEta(new Random().nextInt(maxEta));
            taxis.add(taxi);
        }

        Collections.sort(taxis, new Comparator<Taxi>() {
            public int compare(Taxi one, Taxi other) {
                return one.getEta() - other.getEta();
            }
        });

        return taxis;
    }
}
