package me.rankov.taxilist;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView taxiList = findViewById(R.id.taxiList);
        LinearLayoutManager taxiListManager = new LinearLayoutManager(this);
        DividerItemDecoration divider = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);

        List<Taxi> taxis = Taxi.createStubList();
        TaxiAdapter taxiListAdapter = new TaxiAdapter(taxis);
        taxiListAdapter.refreshStubWithDelay();
        taxiList.setHasFixedSize(true);
        taxiList.addItemDecoration(divider);
        taxiList.setLayoutManager(taxiListManager);
        taxiList.setAdapter(taxiListAdapter);
    }
}
