package me.rankov.taxilist;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import java.util.concurrent.TimeUnit;

class TaxiAdapter extends RecyclerView.Adapter<TaxiAdapter.ViewHolder> {

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public TextView eta;
        public ImageView icon;

        public ViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.item_taxi_name);
            eta = view.findViewById(R.id.item_taxi_eta);
            icon = view.findViewById(R.id.item_taxi_icon);
        }
    }

    private List<Taxi> taxis;
    private Handler handler;

    public TaxiAdapter(List<Taxi> taxis) {
        this.taxis = taxis;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View taxiView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_taxi, parent, false);
        return new ViewHolder(taxiView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Taxi taxi = taxis.get(position);
        holder.icon.setImageDrawable(taxi.getIcon());
        holder.name.setText(taxi.getName());
        holder.eta.setText(parseEta(taxi.getEta(), holder.eta.getContext()));
    }

    private String parseEta(int eta, Context context) {
        long hour = TimeUnit.HOURS.toMinutes(1);
        if (eta <= hour) {
            return String.format(context.getString(R.string.eta_minutes), eta);
        } else {
            return String.format(context.getString(R.string.eta_hours_minutes), eta / hour, eta % hour);
        }
    }

    @Override
    public int getItemCount() {
        return taxis.size();
    }

    public void updateTaxiList(List<Taxi> taxis) {
        this.taxis = taxis;
        notifyDataSetChanged();
    }

    public void refreshStubWithDelay() {
        final long delayMillis = TimeUnit.SECONDS.toMillis(5);
        if (handler == null) {
            handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    taxis = Taxi.createStubList();
                    updateTaxiList(taxis);
                    handler.postDelayed(this, delayMillis);
                }
            }, delayMillis);
        }
    }
}
